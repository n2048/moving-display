/**
   use interrupts for a precise motor control
   NormalOpen switches connection:
    - GND - Left Switch Common
    - Pin 2 - Left Switch NO (Normal Open)
    - GND - Right Switch Common
    - Pin 3 - Right Switch NO
    - Pin 8 - Stepper Driver DIR
    - Pin 13 - Stepper Driver STEP
    - PIN 4, GND - ON-OFF Switch from potentiometer
    - 5V, GND, A0 - Potentiometer
*/

int X_STEP_PIN = 13;
int X_DIR_PIN = 8;

int LEFT_BTN_PIN = 2;
int RIGHT_BTN_PIN = 3;
int ENABLE_SWITCH = 4;

int SPEED_CTR_PIN = A0;

// Storage vars
volatile boolean X_STEP = LOW;
volatile boolean X_DIR = LOW;
volatile boolean _EN = HIGH; // Negated Enabled

const int MAX_COUNT = 62;
const int MIN_COUNT = 15;

int COUNT = MAX_COUNT; 


void setup() {

  // initialize serial:
  Serial.begin(9600);

  //set pins as outputs
  pinMode(X_STEP_PIN, OUTPUT);
  pinMode(X_DIR_PIN, OUTPUT);

  pinMode(LEFT_BTN_PIN, INPUT_PULLUP);
  pinMode(RIGHT_BTN_PIN, INPUT_PULLUP);
  pinMode(ENABLE_SWITCH, INPUT_PULLUP);

  attachInterrupt(digitalPinToInterrupt(LEFT_BTN_PIN), leftBtnPushed, FALLING);
  attachInterrupt(digitalPinToInterrupt(RIGHT_BTN_PIN), rightBtnPushed, FALLING);

  cli();//stop interrupts

  TCCR1A = 0;// set entire TCCR1A register to 0
  TCCR1B = 0;// same for TCCR1B
  TCNT1  = 0;//initialize counter value to 0

  // 1000 ms (1 Hz)
  // OCR1A = 62500; // (must be < 65536)

  // 100 ms (10 Hz)
  OCR1A = 6250; // (must be < 65536)

  // 10 ms (100 Hz)
  // OCR1A = 625; // (must be < 65536)

  // ˜1 ms (˜1 kHz)
  // OCR1A = 62; // (must be < 65536)


  // turn on CTC mode
  TCCR1B |= (1 << WGM12);

  // 256 prescaler
  TCCR1B |= (1 << CS12);
  TCCR1B &= ~(1 << CS11);
  TCCR1B &= ~(1 << CS10);

  // enable timer compare interrupt
  TIMSK1 |= (1 << OCIE1A);

  sei();//allow interrupts

}//end setup


ISR(TIMER1_COMPA_vect) { //timer1 interrupt
  if (X_STEP && !_EN) {
    //  if (X_STEP) {
    digitalWrite(X_STEP_PIN, HIGH);
    //    X_STEP = LOW;
  }
  else {
    digitalWrite(X_STEP_PIN, LOW);
    //    X_STEP = HIGH;
  }
  X_STEP = !X_STEP;
}

void leftBtnPushed() {
  X_DIR = LOW;
  digitalWrite(X_DIR_PIN, X_DIR);
  Serial.println("Left limit switch reached");
}

void rightBtnPushed() {
  X_DIR = HIGH;
  digitalWrite(X_DIR_PIN, X_DIR);
  Serial.println("Right limit switch reached");
}


void loop() {

  boolean NEW_EN = digitalRead(ENABLE_SWITCH);
  if (NEW_EN != _EN) {
    if (NEW_EN) {
      Serial.println("DISABLED");
    }
    else {
      Serial.println("ENABLED");
    }
    _EN = NEW_EN;
  }

  int analogVal = analogRead(SPEED_CTR_PIN);
  int stepperSpeed = map(analogVal, -1, 1024, MAX_COUNT, MIN_COUNT);
  if (stepperSpeed != COUNT) {
    cli();//stop interrupts
    COUNT = stepperSpeed;
    OCR1A = COUNT; // (must be < 65536)
    sei();//allow interrupts
  }

}